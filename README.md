# Jetson Tools

## nvresizefs.sh
Resize root partition to max size

## jetson-fan-ctl-master
Script to control the fan on the jetson (tested nano)

## installSwapfile
Script to create a "swap file" which enables more memory on the nano
